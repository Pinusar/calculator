<?php
require_once('Installment.php');

class CarPolicy
{
    private $carValueInCents;
    private $taxRate;
    private $basePricePercentage;

    /**
     * CarPolicy constructor.
     *
     * @param $carValue
     * @param $taxRate
     */
    public function __construct($carValue, $taxRate, $basePricePercentage)
    {
        $this->carValueInCents = round($carValue, 2) * 100;
        $this->taxRate = $taxRate;
        $this->basePricePercentage = $basePricePercentage;
    }

    public function calculateTotal()
    {
        $basePrice = $this->calculateBasePrice();
        $commission = $this->calculateCommission($basePrice);
        $tax = $this->calculateTax($basePrice);
        return number_format(($basePrice + $commission + $tax) / 100, 2);
    }

    public function calculateInstallments($numOfInstallments)
    {
        $basePrice = $this->calculateBasePrice();
        $commission = $this->calculateCommission($basePrice);
        $tax = $this->calculateTax($basePrice);

        $basePricePerInstallment = intdiv($basePrice, $numOfInstallments);
        $commissionPerInstallment = intdiv($commission, $numOfInstallments);
        $taxPerInstallment = intdiv($tax, $numOfInstallments);

        $basePriceRemainder = $basePrice - ($basePricePerInstallment * $numOfInstallments);
        $commissionRemainder = $commission - ($commissionPerInstallment * $numOfInstallments);
        $taxRemainder = $tax - $taxPerInstallment * $numOfInstallments;


        $installments = [];

        for ($i = 1; $i <= $numOfInstallments; $i++) {
            if ($i == $numOfInstallments) {
                $basePricePerInstallment += $basePriceRemainder;
                $commissionPerInstallment += $commissionRemainder;
                $taxPerInstallment += $taxRemainder;
            }
            $installment = new Installment($basePricePerInstallment, $commissionPerInstallment, $taxPerInstallment);
            $installments[] = $installment;
        }

        return $installments;
    }

    public function getBasePrice()
    {
        return number_format($this->calculateBasePrice() / 100, 2);
    }

    public function getCommission()
    {
        $basePrice = $this->calculateBasePrice();
        return number_format($this->calculateCommission($basePrice) / 100, 2);
    }

    public function getTax()
    {
        $basePrice = $this->calculateBasePrice();
        return number_format($this->calculateTax($basePrice) / 100, 2);
    }

    private function calculateBasePrice()
    {
        return ($this->carValueInCents * $this->basePricePercentage) / 100;
    }

    private function calculateCommission($basePrice)
    {
        return round(($basePrice * 17) / 100);
    }

    private function calculateTax($basePrice)
    {
        return round(($basePrice * $this->taxRate) / 100);
    }
}