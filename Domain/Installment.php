<?php


class Installment
{
    public $basePrice;
    public $commission;
    public $tax;
    public $total;

    /**
     * Installment constructor.
     *
     * @param $basePrice
     * @param $commission
     * @param $tax
     */
    public function __construct($basePrice, $commission, $tax)
    {
        $this->basePrice = number_format($basePrice / 100, 2);
        $this->commission = number_format($commission / 100, 2);
        $this->tax = number_format($tax / 100, 2);
        $this->total = number_format(($basePrice + $commission + $tax) / 100, 2);
    }
}