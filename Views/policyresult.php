<?php

function renderPolicy($context) {
    $tableHeaderData = generateTableHeaderData($context['installments']);
    $emptyCells = generateEmptyCells($context['installments']);
    $basePriceTableData = generatebasePriceTableData($context['installments']);
    $commissionTableData = generatecommissionTableData($context['installments']);
    $taxTableData = generatetaxTableData($context['installments']);
    $totalTableData = generatetotalTableData($context['installments']);
    $carValue = $context['carValue'];
    $taxRate = $context['taxRate'];
    $numOfInstallments = $context['numOfInstallments'];
    $basePrice = $context['basePrice'];
    $commission = $context['commission'];
    $tax = $context['tax'];
    $total = $context['total'];

    echo '<head>
<link rel="stylesheet" href="../css/index.css">
</head>';
    echo "<h1>Policy price calculation</h1>";
    echo "<p>Car value: $carValue. Tax rate: $taxRate. Installments: $numOfInstallments.</p>";

    echo "<table>
        <th>
            <td>Total</td>
            $tableHeaderData
        </th>
        <tr>
            <td>Value</td>
            <td>$carValue</td>
            $emptyCells
        </tr>
        <tr>
            <td>Base price</td>
            <td>$basePrice</td>
            $basePriceTableData
        </tr>
        <tr>
            <td>Commission</td>
            <td>$commission</td>
            $commissionTableData
        </tr>
        <tr>
            <td>Tax</td>
            <td>$tax</td>
            $taxTableData
        </tr>
        <tr>
            <td>Total</td>
            <td><strong>$total</strong></td>
            $totalTableData
        </tr>
     </table>";

    echo "<a href='../index.php'>Back to main</a>";
}

function generateTableHeaderData($installments)
{
    $result = "";
    for ($i = 1; $i <= count($installments); $i++) {
        $result .= "<td>$i installment</td>";
    }
    return $result;
}

function generatebasePriceTableData($installments)
{
    $result = "";
    foreach ($installments as $installment) {
        $result .= "<td>$installment->basePrice</td>";
    }
    return $result;
}

function generatecommissionTableData($installments)
{
    $result = "";
    foreach ($installments as $installment) {
        $result .= "<td>$installment->commission</td>";
    }
    return $result;
}

function generatetaxTableData($installments)
{
    $result = "";
    foreach ($installments as $installment) {
        $result .= "<td>$installment->tax</td>";
    }
    return $result;
}

function generatetotalTableData($installments)
{
    $result = "";
    foreach ($installments as $installment) {
        $result .= "<td>$installment->total</td>";
    }
    return $result;
}

function generateEmptyCells($installments)
{
    $result = "";
    foreach ($installments as $installment) {
        $result .= "<td></td>";
    }
    return $result;
}