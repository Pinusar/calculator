<?php
require_once('../Domain/CarPolicy.php');
require_once('../Domain/Installment.php');
require_once('../Views/policyresult.php');

$carValue = $_GET["carValue"];
$taxRate = $_GET["taxRate"];
$numOfInstallments = $_GET["installments"];
$time = $_GET["time"];

$dt = new DateTime(substr($time, 0, strpos($time, '(')));
$day = $dt->format('D');
$hour = $dt->format('H');
if ($day === 'Fri' && $hour >= 15 && $hour < 20) {
    $basePricePercentage = 13;
} else {
    $basePricePercentage = 11;
}

$policy = new CarPolicy($carValue, $taxRate, $basePricePercentage);
$total = $policy->calculateTotal();
$basePrice = $policy->getBasePrice();
$commission = $policy->getCommission();
$tax = $policy->getTax();

$installments = $policy->calculateInstallments($numOfInstallments);

$context = [];
$context['installments'] = $installments;
$context['carValue'] = number_format($carValue, 2);
$context['taxRate'] = $taxRate;
$context['numOfInstallments'] = $numOfInstallments;
$context['basePrice'] = $basePrice;
$context['commission'] = $commission;
$context['tax'] = $tax;
$context['total'] = $total;

renderPolicy($context);
